# Sobre o Projeto
Projeto criado como solução para o Game-of-life do matemático John Horton Conway, teste apresentado pela empresa Captalys.

Sistema criado e progamado em hambiênte Windows com Pycharm.

# Bibliotecas Instalaveis

pygame

# Instalando o pygame no Pycharm Windows

Primeiro use o comando "import pygame", clique na lampada da imagem abaixo:
![img.png](img.png)

Em seguida clique em "install package pygame":
![img_1.png](img_1.png)

Aguarde o termino da instalação:
![img_2.png](img_2.png)

O projeto já tem tudo o que precisa para funcionar.

# Execução do programa via Pycharm

Utilize o atalhode teclado utilize "Ctrl+Shift+F10", ou clique no simbolo demarcado na imagem abaixo:
![img_3.png](img_3.png)

# Programa em Funcionamento

print simples:
![img_4.png](img_4.png)

# Referências

## Pygame
https://humberto.io/pt-br/blog/desbravando-o-pygame-3-game-loop/

https://www.pygame.org/docs/

https://www-pygame-org.translate.goog/docs/ref/display.html?_x_tr_sl=en&_x_tr_tl=pt&_x_tr_hl=pt-BR&_x_tr_pto=nui,sc,elem

https://www-pygame-org.translate.goog/docs/ref/draw.html?_x_tr_sl=en&_x_tr_tl=pt&_x_tr_hl=pt-BR&_x_tr_pto=nui,sc,elem


## Cores
https://www.rapidtables.com/web/color/RGB_Color.html#color-table

## Creditos

### Curso em Vídeo
Professor Gustavo Guanabara

### Canal no Youtube
https://www.youtube.com/c/CursoemV%C3%ADdeo/featured
### Play List
https://www.youtube.com/playlist?list=PLvE-ZAFRgX8hnECDn1v9HNTI71veL3oW0
