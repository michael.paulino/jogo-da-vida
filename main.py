import pygame
import random
import time

from l_jogo import jogo_da_vida

vida = [[random.randint(0, 1) for _ in range(50)] for _ in range(50)] #plano randônico

#Estou definindo que semente como um dos valores acima
semente = vida
#iniciando a biblioteca pygame para dar forma ao jogo
pygame.init()

pygame.display.set_caption("Jogo da Vida - Michael_Edition")#Dando nome ao meu jogo
#metodo para aplicar resolução no jogo de acordo com seu display ou não
tela = pygame.display.set_mode((550, 550))

#Desenhando o plano
def des_matriz(matriz):

    tela.fill([255, 255, 255])
    #percorre o plano desenhando as celulas com os valores
    for x, linha in enumerate(matriz):
        for y, celula in enumerate(linha):
            # se a celula estiver viva, pinte de vermelho.
            if celula:
                pygame.draw.rect(tela, (200, 0, 0), (11 * y, 11 * x, 10, 10))


#Dando forma ao inicio da nossa semente
des_matriz(semente)
pygame.display.flip()

time.sleep(1)

while True:

    evento = pygame.event.poll()

    if evento.type == pygame.QUIT:
        break #Finaliza o programa se o usuário clicar em fechar

    #Atuzalização do jogo, colocando o jogo da vida para processar a proxima geração
    semente = jogo_da_vida(semente)

    #Desenhando a nova geração na tela
    des_matriz(semente)
    pygame.display.flip()
    #função para determinar quantos segundos demora para mudar de ciclo
    time.sleep(0.1)
