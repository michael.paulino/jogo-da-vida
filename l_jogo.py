def chec_cell(secao):

    vizinho = 0 #contador de vizinhos
    centro = secao[1][1] #centro do recorte

    for linha in secao: #somando todos os elementos do grupo
        for celula in linha:
            vizinho = vizinho + celula

    vizinho = vizinho - centro

    #condições basicas do jogo da vida
    if vizinho <=1:
        centro = 0 #se o número de vizinhos for 1 ele morre de solidão

    elif vizinho == 3:
        centro = 1 #se a quantidade de vizinhos forem 3 a celula se reproduz

    elif vizinho >= 4:
        centro = 0 #Se a quantidade de vizinhos for mais do que 3 a célula morre de super população

    return centro #retorna o valor da célula centro

def get_secao(matriz, linha, coluna):
    #Montando um plano 3x3 com celulas mortas para uma cópia
    secao = [[0 for _ in range(3)] for _ in range(3)]

    # função para percorrer as redondezas da célula de linha e coluna, copiando o valor para seção
    for sec_x, x in enumerate(range(linha-1, linha+2)):
        for sec_y, y in enumerate(range(coluna-1, coluna+2)):
            secao[sec_x][sec_y] = matriz[x % 50][y % 50]

    return secao #devolve a informação 3x3 do plano

def jogo_da_vida(semente):
    #Criando um plano vazinho para armazenar a nova geração
    prox_geracao = [[1 for _ in range(50)] for _ in range(50)]
    #percorre o plano, tirando recortes 3x3 da vizinhança da celula central
    for x, linha in enumerate(semente):
        for y, coluna in enumerate(linha):
            #avalia para descobrir a geração seguinte de cada celula
            prox_geracao[x][y] = chec_cell(get_secao(semente, x, y))

    return prox_geracao #devolve a proxima geração